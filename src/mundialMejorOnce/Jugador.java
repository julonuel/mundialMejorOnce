package mundialMejorOnce;


import java.util.ArrayList;

public class Jugador {
	
	private String nombre;
	private String  nacionalidad;
	private ArrayList<String> posicionesQueJuega;
	private int cantidadDeGoles;
	private int cantidadDeFaltas;
	private int cantidadDeTarjetasRecibidas;
	private double promedioPuntaje;
	private double coeficiente;
	/*
	 * Constructor de la clase.
	 */
	public Jugador(String nombre, String nacionalidad, ArrayList<String> posicionesQueJuega, int cantidadDeGoles, 
	int cantidadDeFaltas, int cantidadDeTarjetasRecibidas, double promedioPuntaje) {
		this.nombre = nombre;
		this.nacionalidad = nacionalidad;
		this.posicionesQueJuega = posicionesQueJuega;
		this.cantidadDeGoles = cantidadDeGoles;
		this.cantidadDeFaltas = cantidadDeFaltas;
		this.cantidadDeTarjetasRecibidas = cantidadDeTarjetasRecibidas;
		this.promedioPuntaje = promedioPuntaje;
		this.coeficiente=this.calcularCoeficiente();
	}
	/*
	 * Seccion de getters y setters de las variables de instancia,
	 */
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	
	public ArrayList<String> getPosicionesQueJuega() {
		return posicionesQueJuega;
	}

	public void setPosicionesQueJuega(ArrayList<String> posicionesQueJuega) {
		this.posicionesQueJuega = posicionesQueJuega;
	}

	public int getCantidadDeGoles() {
		return cantidadDeGoles;
	}

	public void setCantidadDeGoles(int cantidadDeGoles) {
		this.cantidadDeGoles = cantidadDeGoles;
	}

	public int getCantidadDeFaltas() {
		return cantidadDeFaltas;
	}

	public void setCantidadDeFaltas(int cantidadDeFaltas) {
		this.cantidadDeFaltas = cantidadDeFaltas;
	}

	public int getCantidadDeTarjetasRecibidas() {
		return cantidadDeTarjetasRecibidas;
	}

	public void setCantidadDeTarjetasRecibidas(int cantidadDeTarjetasRecibidas) {
		this.cantidadDeTarjetasRecibidas = cantidadDeTarjetasRecibidas;
	}

	public double getPromedioPuntaje() {
		return promedioPuntaje;
	}

	public void setPromedioPuntaje(double promedioPuntaje) {
		this.promedioPuntaje = promedioPuntaje;
	}

	public double getCoeficiente() {
		this.coeficiente = calcularCoeficiente();
		return coeficiente;
	}

	public void setCoeficiente(double coeficiente) {
		this.coeficiente = coeficiente;
	}
	/*
	 * metodo double calcularCoeficiente.
	 * Devuelve el coeficiente correspondiente a este jugador, en base a la formula que le ponga. En caso de querer cambiar
	 * la formula, solo deberia cambiar este metodo.
	 */
	private double calcularCoeficiente() {
		return 2*this.cantidadDeGoles-(this.cantidadDeFaltas/10)-this.cantidadDeTarjetasRecibidas+this.promedioPuntaje;
	}
	
	@Override
	public String toString() {
		return "Nombre: "+nombre+"\nNacionalidad: "+this.nacionalidad+"\nPosiciones que juega: "+posicionesQueJuega.toString()+"\nCantidad de goles: "
		+cantidadDeGoles+"\nCantidad de faltas: "+cantidadDeFaltas+"\nCantidad de tarjetas recibidas: "+cantidadDeTarjetasRecibidas+"\nPromedio puntaje: "
		+promedioPuntaje+"\nCoeficiente: "+coeficiente+".";
	}
	
	@Override
	public boolean equals(Object jugador) {
		return this.nombre.equals(((Jugador)jugador).nombre) && this.nacionalidad.equals(((Jugador)jugador).nacionalidad);
	}
}
