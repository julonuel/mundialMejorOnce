package mundialMejorOnce;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JOptionPane;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
/* Clase Seleccionador.
 * Se encarga de devolver el mejor 11 de todos los jugadores de los que quiero revisar.
 */
public class Seleccionador {
	/* Variables de instancia.
	 * La variable candidatos seran todos los jugadores de los que quiero seleccionar el mejor 11.
	 * La variable candidatosOrdenados tendra 11 arraylist, donde en el primero, por ejemplo, estaran todos los arqueros candidatos, ordenados del mejor al peor. 
	 */
	private ArrayList<Jugador> candidatos;
	private ArrayList<ArrayList<Jugador>> candidatosOrdenados;
	private ArrayList<String> selecciones;
	//private ArrayList<Jugador>equipoIdeal;
	
	/* public Seleccionador()
	 * Constructor con los candidatos vacios.
	 */
	public Seleccionador() {
		this.candidatos = new ArrayList<Jugador>();
		this.candidatosOrdenados = new ArrayList<ArrayList<Jugador>>();
		this.selecciones = new ArrayList<String>();
	}
	/* public Seleccionador(ArrayList<Jugador> candidatos)
	 * Constructor con los candidatos pasados como parametro.
	 */
	public Seleccionador(ArrayList<Jugador> candidatos) {
		this.setCandidatos(candidatos);
		this.candidatosOrdenados = new ArrayList<ArrayList<Jugador>>();
		this.selecciones = new ArrayList<String>();
	}
	/* public void agregarJugador(Jugador j)
	 * Agrega un jugador a los candidatos, siempre y cuando se pueda agregar.
	 */
	public void agregarJugador(Jugador j){
		if(!existeJugador(j)){
			if(cantJugadoresDeSeleccion(j.getNacionalidad())<23){
				this.candidatos.add(j);
			}else{
				throw new RuntimeException("error al agregar jugador: supera el limite de 23 jugadores");
			}
		}else{
			throw new RuntimeException("error al agregar jugador: el jugador que se desea agregar ya existe");
		}
	}
	/* public void agregarSeleccion(String s)
	 * Agregar una seleccion a todas las disponibles.
	 */
	public void agregarSeleccion(String s){
		if(!this.selecciones.contains(s)){
			if(this.selecciones.size()<32){
				this.selecciones.add(s);
			}else{
				throw new RuntimeException("error al agregar seleccion: supera el limite de 32 selecciones inscriptas por mundial");
			}
		}else{
			throw new RuntimeException("error al agregar seleccion: la seleccion que se desea agregar ya existe");
		}
	}
	/* 
	 * Sector de getter y setters de las variables de instancia de la clase.
	 */
	public ArrayList<Jugador> getCandidatos() {
		return candidatos;
	}

	public void setCandidatos(ArrayList<Jugador> candidatos) {
		this.candidatos = candidatos;
	}

	public ArrayList<ArrayList<Jugador>> getCandidatosOrdenados() {
		this.candidatosOrdenados = this.ordenarCandidatos(candidatos);
		return candidatosOrdenados;
	}

	public void setCandidatosOrdenados(ArrayList<ArrayList<Jugador>> candidatosOrdenados) {
		this.candidatosOrdenados = candidatosOrdenados;
	}
	
	public ArrayList<String> getSelecciones(){
		return selecciones;
	}
	
	public void setSelecciones(ArrayList<String> s){
		this.selecciones = s;
	}
	
	
	/*el metodo existeJugador(Jugador j) retorna un 
	**booleano de si existe o no el jugador pasado
	**por parametro en la lista de candidatos
	*/
	public boolean existeJugador(Jugador j){
		boolean yaExiste = false;
		for(int i=0;i<this.candidatos.size();i++){
			if(this.candidatos.get(i).equals(j)){
				yaExiste = true;
			}
		}
		return yaExiste;
	}
	
	/*el metodo cantJugadoresDeSeleccion(String seleccion)
	**retorna un int con la cantidad de jugadores que hay
	**de una determinada seleccion pasada por parametro
	*/
	public int cantJugadoresDeSeleccion(String seleccion){
		int cantidadJugadores = 0;
		for(int p=0;p<this.candidatos.size();p++){
			if(this.candidatos.get(p).getNacionalidad().equals(seleccion)){
				cantidadJugadores++;
			}
		}
		return cantidadJugadores;
	}
	
	/* Metodo ordenar candidatos. 
	 * Se encarga de setear la variable candidatos ordenados, este seteo consiste en: 
	 * Agregar en arrays separados segun la posicion en que juega cada jugador, y despues ordenarlos de mejor a peor jugador, gracias al comparador; y por ultimo
	 * agregarlos a la variable que sera devuelta.
	 */
	public  ArrayList<ArrayList<Jugador>> ordenarCandidatos(ArrayList<Jugador> candidatos) {
		ArrayList<ArrayList<Jugador>> ret=new ArrayList<>();
		String[] todasLasPosicionesPosibles = {"Arquero","Lateral izquierdo","Central izquierdo","Central derecho","Lateral derecho","Volante izquierdo",
		"Volante central","Volante derecho","Puntero izquierdo","Centrodelantero","Puntero derecho"};
		
		for(String posicion : todasLasPosicionesPosibles) {
			ArrayList<Jugador> jugadoresPosicion = new ArrayList<>();
			for(Jugador jugador : candidatos) {
				if(jugador.getPosicionesQueJuega().contains(posicion)) {
					jugadoresPosicion.add(jugador);
				}
			}
			Collections.sort(jugadoresPosicion, new Comparador());
			ret.add(jugadoresPosicion);
		}
		
		return ret;
	}
	/* public void guardar(String archivo)
	 * Se encarga de guardar un archivo json que dise�o el usuario, lo guardara con el nombre pasado como parametro a la funcion.
	 */
	public void guardar(String archivo){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		try{
			FileWriter writer = new FileWriter(archivo);
			writer.write(json);
			writer.close();
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, e.toString());
		}
	}
	/* public static Seleccionador cargarArchivoGuardado(String archivo)
	 * Devuelve un seleccionador con el json cargado, de la ruta que le paso como parametro.
	 */
	public static Seleccionador cargarArchivoGuardado(String archivo){
		Gson gson = new Gson();
		Seleccionador ret = null;
		try{
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, Seleccionador.class);
		}catch(FileNotFoundException e){
			JOptionPane.showMessageDialog(null, "error no encuentra el archivo");
		}catch(Exception e){
			JOptionPane.showMessageDialog(null,e.toString());
		}
			return ret;
	}
	/* public ArrayList<Jugador> dameTodosJugadoresDeSeleccion(String nacionalidad)
	 * Devuelve un arraylist de jugadores que son de la nacionalidad pasada como parametro. Se usa para la tabla que se muestra en la interfaz grafica.
	 */
	public ArrayList<Jugador> dameTodosJugadoresDeSeleccion(String nacionalidad){
		ArrayList<Jugador> conjuntoSelec = new ArrayList<>();
		for(Jugador j : this.candidatos){
			if(j.getNacionalidad().equals(nacionalidad)){
				conjuntoSelec.add(j);
			}
		}
		return conjuntoSelec;
	}
	
	/*mejorOnce
	 * Devuelve el mejor once de todos los candidatos pasados como parametro. Es el algoritmo goloso. Va a agregar a los mejores en el orden de los arrays de la 
	 * variable de candidatos ordenados.   
	 */
	public ArrayList<Jugador> mejorOnce() {
		ArrayList<Jugador> mejorOnce = new ArrayList<>();
		for(int i = 0; i < this.getCandidatosOrdenados().size(); i++) {
			for(int j = 0; j < this.getCandidatosOrdenados().get(i).size();j++) {
				if(loPuedoAgregar(mejorOnce, this.getCandidatosOrdenados().get(i).get(j))) {
					mejorOnce.add(this.getCandidatosOrdenados().get(i).get(j));
					j = this.getCandidatosOrdenados().get(i).size();
				}
			}
			
		}
		return mejorOnce;
	}
	/* loPuedoAgregar 
	 * Chequea si a un jugador lo puedo agregar al mejor once, es decir, que cumpla con todas las  restricciones.
	 */
	private static boolean loPuedoAgregar(ArrayList<Jugador> mejorOnce, Jugador jugador) {
		boolean res = false;
		if(!mejorOnce.contains(jugador) && (cantJugadoresDe(mejorOnce, jugador.getNacionalidad()) < 4)) {
			if (jugador.getCantidadDeTarjetasRecibidas() > 0 && cantJugadoresConTarjetas(mejorOnce) < 5) {
				if(jugador.getCantidadDeGoles() == 0 && cantJugadoresSinGoles(mejorOnce) < 6) {
					res = true; 
				}else if (jugador.getCantidadDeGoles()>0) {
					res = true;
				}
			}else if(jugador.getCantidadDeTarjetasRecibidas() == 0){
				res = true;
			}
		}
		return res;
	}
	/* cantJugadoresDe 
	 * Devuelve la cantidad de jugadores de la nacionalidad pasada como parametro, en el arreglo de jugadores, lo voy a usar para chequear las restricciones del
	 * mejor once. 
	 */
	private static int cantJugadoresDe(ArrayList<Jugador>jugadores, String nacionalidad) {
		int res=0;
		for(Jugador jugador : jugadores) {
			if(jugador.getNacionalidad().equals(nacionalidad)) {
				res++;
			}
		}
		return res;
	}
	/* cantJugadoresConTarjetas
	 * Devuelve la cantidad de jugadores que tienen tarjetas del arraylist de jugadores, lo voy a usar para chequear si lo puedo agregar al mejor once.
	 */
	private static int cantJugadoresConTarjetas(ArrayList<Jugador> jugadores) {
		int res=0;
		for(Jugador jugador : jugadores) {
			if(jugador.getCantidadDeTarjetasRecibidas()>0) {
				res++;
			}
		}
		return res;
	}
	/* cantJugadoresSinGoles 
	 * Devuelve la cantidad de jugadores que no tienen goles en el arraylist de jugadores, lo voy a usar para controlar si a un jugador lo puedo agregar al mejor once.
	 */
	private static int cantJugadoresSinGoles(ArrayList<Jugador> jugadores) {
		int res=0;
		for(Jugador jugador : jugadores) {
			if(jugador.getCantidadDeGoles()==0) {
				res++;
			}
		}
		return res;
	}
	/* public int cantJugadoresOrdenados()
	 * Cuenta la cantidad de jugadores que hay en cada arraylist de los candidatos ordenados, luego le resta 11 que son los cambios de cada arraylist,
	 * segun posicion, sirve para testear si estan bien ordenados. 
	 */
	public int cantJugadoresOrdenados() {
		int res = 0;
		for(ArrayList<Jugador> jugadores : this.candidatosOrdenados) {
			res += jugadores.size();
		}
		return res-11;
	}
}
