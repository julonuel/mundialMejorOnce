package mundialMejorOnce;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class LectorDeDatos {
	Seleccionador seleccionador;

	public LectorDeDatos(Seleccionador seleccionador) {
		this.seleccionador = seleccionador;
	}
	
	public Seleccionador getSeleccionador() {
		return this.seleccionador;
	}
	
	public void setSeleccionador(Seleccionador seleccionador) {
		this.seleccionador = seleccionador;
	}
	
	public ArrayList<String> getDatosDe(String direccion) {
		ArrayList<String> res = new ArrayList<>();
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;
		try {
			archivo = new File (direccion);
			fr = new FileReader (archivo);
			br = new BufferedReader(fr);

         String linea;
         while((linea=br.readLine())!=null)
            res.add(linea);
      }
      catch(Exception e){
         e.printStackTrace();
      }finally{
         try{
            if( null != fr ){
               fr.close();
            }
         }catch (Exception e2){
            e2.printStackTrace();
         }
      }
		return res;
	}
	
	public void setDatos(String nacionalidad) {
		String [] finalRuta = {"\\Faltas.txt", "\\Goles.txt","\\Nombres.txt","\\Posiciones.txt","\\Puntaje promedio.txt","\\Tarjetas.txt"};
		
		ArrayList<String> faltas = null;
		ArrayList<String> goles = null;
		ArrayList<String> nombres = null;
		ArrayList<String> posiciones;
		ArrayList<ArrayList<String>> posicionesOrdenadas = null;
		ArrayList<String> promedio = null;
		ArrayList<String> tarjetas = null;	
		
		for(String finalR : finalRuta) {
			String ruta = new File ("").getAbsolutePath();
			ruta += "\\Datos\\Base1\\" + nacionalidad; 
			if(finalR.equals("\\Faltas.txt")) {
				ruta += finalR;
				faltas = getDatosDe(ruta);
			}
			if(finalR.equals("\\Goles.txt")) {
				ruta += finalR;
				goles = getDatosDe(ruta);
			}
			if(finalR.equals("\\Nombres.txt")) {
				ruta += finalR;
				nombres = getDatosDe(ruta);
			}
			if(finalR.equals("\\Posiciones.txt")) {
				ruta += finalR;
				posiciones = getDatosDe(ruta);
				posicionesOrdenadas = separarPosiciones(posiciones);
			}
			if(finalR.equals("\\Puntaje promedio.txt")) {
				ruta += finalR;
				promedio = getDatosDe(ruta);
			}
			if(finalR.equals("\\Tarjetas.txt")) {
				ruta += finalR;
				tarjetas = getDatosDe(ruta);
			}
		}
		for(int j = 0;  j< nombres.size();j++) {
			Jugador jugador = new Jugador(nombres.get(j), nacionalidad, posicionesOrdenadas.get(j),
			Integer.parseInt(goles.get(j)),Integer.parseInt(faltas.get(j)), Integer.parseInt(tarjetas.get(j)), Double.parseDouble(promedio.get(j)));
			this.seleccionador.agregarJugador(jugador);
		}
		this.seleccionador.setCandidatosOrdenados(this.seleccionador.ordenarCandidatos(this.seleccionador.getCandidatos()));
	}

	private ArrayList<ArrayList<String>> separarPosiciones(ArrayList<String> posiciones) {
		ArrayList<ArrayList<String>> posicionesOrdenadas = new ArrayList<>();
		Pattern pat = Pattern.compile(",");
		for(int i = 0 ; i<posiciones.size(); i++) {
			String[] posicionesJ = pat.split(posiciones.get(i));
			ArrayList<String> posicionesJugador = new ArrayList<>();
			for(int j = 0 ; j<posicionesJ.length;j++) {
				posicionesJugador.add(posicionesJ[j]);
			}
			posicionesOrdenadas.add(posicionesJugador);
		}
		return posicionesOrdenadas;
	}
 }