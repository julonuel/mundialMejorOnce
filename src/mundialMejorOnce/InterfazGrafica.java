package mundialMejorOnce;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.border.MatteBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTabbedPane;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;

public class InterfazGrafica  {

	private JPanel contentPane;
	private JTabbedPane tabbedPane;
	private DefaultTableCellRenderer dtcr;
	private String[] cabeceraTabla;
	private String[][] datosTablaMejorOnce;
	private String[][] datosTablaSeleccion;
	private JTable tablaSeleccion;
	private JTable tablaMejorXI;
	private JFrame framePrincipal;
	private JFrame frameAgregarJugador;
	private Seleccionador seleccionador;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazGrafica frame = new InterfazGrafica();
					frame.getFramePrincipal().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public InterfazGrafica() {
		
		seleccionador = new Seleccionador();
		
		framePrincipal = new JFrame();
		getFramePrincipal().setResizable(false);
		getFramePrincipal().setIconImage(Toolkit.getDefaultToolkit().getImage(InterfazGrafica.class.getResource("/imagenes/copa_icon.png")));
		getFramePrincipal().setTitle("Equipo Ideal del Mundial");
		getFramePrincipal().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFramePrincipal().setBounds(100, 100, 800, 480);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		getFramePrincipal().setContentPane(contentPane);
		contentPane.setLayout(null);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(444, 135, 298, 227);
		contentPane.add(tabbedPane);
		
		JScrollPane scrollPane = new JScrollPane();
		tabbedPane.addTab("MejorXI", null, scrollPane, null);
		
		scrollPane.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			
		cabeceraTabla = new String[] {"Nombre", "Goles", "Faltas", "Tarjetas",};
		
		datosTablaMejorOnce = new String[][] {{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, 
										 {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},};
		
		DefaultTableModel modelo = new DefaultTableModel();														
		tablaMejorXI = new JTable(modelo);
		tablaMejorXI.setModel(new DefaultTableModel(datosTablaMejorOnce, cabeceraTabla));
		configurarTabla(tablaMejorXI);
		
		scrollPane.setViewportView(tablaMejorXI);

		JLabel lblCoeficienteTotal = new JLabel("Coeficiente Total");
		lblCoeficienteTotal.setForeground(Color.WHITE);
		lblCoeficienteTotal.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		lblCoeficienteTotal.setHorizontalAlignment(SwingConstants.CENTER);
		lblCoeficienteTotal.setBounds(633, 22, 129, 26);
		contentPane.add(lblCoeficienteTotal);
		
		JLabel lblCoefNum = new JLabel("0.0");
		lblCoefNum.setForeground(new Color(255, 69, 0));
		lblCoefNum.setFont(new Font("Trebuchet MS", Font.PLAIN, 42));
		lblCoefNum.setHorizontalAlignment(SwingConstants.CENTER);
		lblCoefNum.setBounds(636, 52, 130, 40);
		contentPane.add(lblCoefNum);
		
		JLabel lblCoefPuntDer = new JLabel();
		editarLabel(lblCoefPuntDer, getFramePrincipal(), 84, 299, 24, 10, "", 255, 255, 255);
		
		JLabel lblCoefCentDel = new JLabel();
		editarLabel(lblCoefCentDel, getFramePrincipal(), 227, 309, 24, 10, "", 255, 255, 255);
	
		JLabel lblCoefPuntIzq = new JLabel();
		editarLabel(lblCoefPuntIzq, getFramePrincipal(), 362, 299, 24, 10, "", 255, 255, 255);
		
		JLabel lblCoefVolDer = new JLabel();
		editarLabel(lblCoefVolDer, getFramePrincipal(), 120, 194, 24, 10, "", 255, 255, 255);
		
		JLabel lblCoefVolCent = new JLabel();
		editarLabel(lblCoefVolCent, getFramePrincipal(), 227, 194, 24, 10, "", 255, 255, 255);
		
		JLabel lblCoefVolIzq = new JLabel();
		editarLabel(lblCoefVolIzq, getFramePrincipal(), 344, 194, 24, 10, "", 255, 255, 255);
		
		JLabel lblCoefLatDer = new JLabel();
		editarLabel(lblCoefLatDer, getFramePrincipal(), 55, 81, 24, 10, "", 255, 255, 255);
		
		JLabel lblCoefCentDer = new JLabel();
		editarLabel(lblCoefCentDer, getFramePrincipal(), 146, 79, 24, 10, "", 255, 255, 255);
		
		JLabel lblCoefCentIzq = new JLabel();
		editarLabel(lblCoefCentIzq, getFramePrincipal(), 305, 79, 24, 10, "", 255, 255, 255);
		
		JLabel lblCoefLatIzq = new JLabel();
		editarLabel(lblCoefLatIzq, getFramePrincipal(), 386, 92, 24, 10, "", 255, 255, 255);
		
		JLabel lblCoefArq = new JLabel();
		editarLabel(lblCoefArq, getFramePrincipal(), 231, 11, 24, 10, "", 255, 255, 255);
		
		JLabel lblNombrePuntDer = new JLabel();
		editarLabel(lblNombrePuntDer, getFramePrincipal(), 21, 382, 76, 20, "", 255, 255, 255);
		
		JLabel lblNombreCentDel = new JLabel();
		editarLabel(lblNombreCentDel, getFramePrincipal(), 162, 395, 76, 20, "", 255, 255, 255);
		
		JLabel lblNombrePuntIzq = new JLabel();
		editarLabel(lblNombrePuntIzq, getFramePrincipal(), 299, 382, 76, 20, "", 255, 255, 255);

		JLabel lblNombreVolDer = new JLabel();
		editarLabel(lblNombreVolDer, getFramePrincipal(), 55, 268, 76, 20, "", 255, 255, 255);
		
		JLabel lblNombreVolCent = new JLabel();
		editarLabel(lblNombreVolCent, getFramePrincipal(), 162, 268, 76, 20, "", 255, 255, 255);
		
		JLabel lblNombreVolIzq = new JLabel();
		editarLabel(lblNombreVolIzq, getFramePrincipal(), 278, 268, 76, 20, "", 255, 255, 255);
		
		JLabel lblNombreLatDer = new JLabel();
		editarLabel(lblNombreLatDer, getFramePrincipal(), 0, 169, 76, 20, "", 255, 255, 255);
		
		JLabel lblNombreCentDer = new JLabel();
		editarLabel(lblNombreCentDer, getFramePrincipal(), 82, 154, 76, 20, "", 255, 255, 255);
		
		JLabel lblNombreCentIzq = new JLabel();
		editarLabel(lblNombreCentIzq, getFramePrincipal(), 233, 154, 76, 20, "", 255, 255, 255);
		
		JLabel lblNombreLatIzq = new JLabel();
		editarLabel(lblNombreLatIzq, getFramePrincipal(), 319, 168, 76, 20, "", 255, 255, 255);
		
		JLabel lblNombreArq = new JLabel();	
		editarLabel(lblNombreArq, getFramePrincipal(), 162, 87, 76, 20, "", 255, 255, 255);
		
		JLabel lblPuntDer = new JLabel();
		crearCamiseta(lblPuntDer, 21, 299, 76, 92);
		
		JLabel lblCentDel = new JLabel();
		crearCamiseta(lblCentDel, 162, 310, 76, 92);
		
		JLabel lblPuntIzq = new JLabel();
		crearCamiseta(lblPuntIzq, 299, 299, 76, 92);
		
		JLabel lblVolDer = new JLabel();		
		crearCamiseta(lblVolDer, 55, 184, 76, 92);
		
		JLabel lblVolCent = new JLabel();
		crearCamiseta(lblVolCent, 162, 184, 81, 92);
		
		JLabel lblVolIzq = new JLabel();
		crearCamiseta(lblVolIzq, 276, 182, 81, 94);
		
		JLabel lblLatDer = new JLabel();
		crearCamiseta(lblLatDer, 0, 82, 76, 92);
		
		JLabel lblCentDer = new JLabel();
		crearCamiseta(lblCentDer, 82, 70, 80, 92);
		
		JLabel lblCentIzq = new JLabel();
		crearCamiseta(lblCentIzq, 239, 70, 76, 92);
	
		JLabel lblLatIzq = new JLabel();
		crearCamiseta(lblLatIzq, 319, 82, 76, 92);
		
		JLabel lblArquero = new JLabel();
		crearCamiseta(lblArquero, 162, 0, 76, 92);
		
		JButton btnAgregarJugador = new JButton("Agregar Jugador");
		btnAgregarJugador.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		agregarJugador();
        	}
        });
		editarBoton(btnAgregarJugador, getFramePrincipal(), 452, 368, 120, 23);
		
		JButton btnAgregarSeleccion = new JButton("Agregar Seleccion");
		btnAgregarSeleccion.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		agregarSeleccion();
        	}
        });
		editarBoton(btnAgregarSeleccion, getFramePrincipal(), 452, 410, 120, 23);
		
		JButton btnMejorXiGoloso = new JButton("Mejor XI (Goloso)");
		btnMejorXiGoloso.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		ArrayList<Jugador> mejorXIGoloso = seleccionador.mejorOnce();
        		Double coeficienteMax = new Double(0);
        		int Fjug = 0;
        		for(Jugador jugador : mejorXIGoloso){      			
        			tablaMejorXI.getModel().setValueAt(jugador.getNombre(), Fjug, 0); 
        			tablaMejorXI.getModel().setValueAt(jugador.getCantidadDeGoles(),Fjug, 1);
        			tablaMejorXI.getModel().setValueAt(jugador.getCantidadDeFaltas(), Fjug, 2);
        			tablaMejorXI.getModel().setValueAt(jugador.getCantidadDeTarjetasRecibidas(), Fjug, 3);
        			coeficienteMax = coeficienteMax + jugador.getCoeficiente();
        			Fjug++;
        		}
        		String CoefCort = "";
        		int contD = 0;
        		boolean pasoPunto = false;
        		for(int j=0;j<coeficienteMax.toString().length();j++){
        			if(contD<2){
        			CoefCort = CoefCort+coeficienteMax.toString().charAt(j);
        				if(coeficienteMax.toString().charAt(j)=='.')pasoPunto = true;
        				if(pasoPunto)contD++;
        			}
        		}
        		lblCoefNum.setText(CoefCort);
        		
        		dibujarJugador("arquero", mejorXIGoloso, lblArquero, lblNombreArq, lblCoefArq);
        		dibujarJugador("lateralIzquierdo", mejorXIGoloso, lblLatIzq, lblNombreLatIzq, lblCoefLatIzq);
        		dibujarJugador("centralIzquierdo",mejorXIGoloso, lblCentIzq, lblNombreCentIzq, lblCoefCentIzq);
        		dibujarJugador("centralDerecho",mejorXIGoloso, lblCentDer, lblNombreCentDer, lblCoefCentDer);
        		dibujarJugador("lateralDerecho",mejorXIGoloso, lblLatDer, lblNombreLatDer, lblCoefLatDer);
        		dibujarJugador("volanteIzquierdo",mejorXIGoloso, lblVolIzq, lblNombreVolIzq, lblCoefVolIzq);
        		dibujarJugador("volanteCentral",mejorXIGoloso, lblVolCent, lblNombreVolCent, lblCoefVolCent);
        		dibujarJugador("volanteDerecho",mejorXIGoloso, lblVolDer, lblNombreVolDer, lblCoefVolDer);
        		dibujarJugador("punteroIzquierdo",mejorXIGoloso, lblPuntIzq, lblNombrePuntIzq, lblCoefPuntIzq);
        		dibujarJugador("centroDelantero",mejorXIGoloso, lblCentDel, lblNombreCentDel, lblCoefCentDel);
        		dibujarJugador("punteroDerecho",mejorXIGoloso, lblPuntDer, lblNombrePuntDer, lblCoefPuntDer);
        	}
        });
		editarBoton(btnMejorXiGoloso, getFramePrincipal(), 590, 368, 120, 23);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 60, 21);
		contentPane.add(menuBar);
		
		JMenu mnMenu = new JMenu("Archivo");
		menuBar.add(mnMenu);
		
		JMenuItem mnMenuItemCargar = new JMenuItem("Cargar");
		mnMenuItemCargar.setSelected(true);
		mnMenu.add(mnMenuItemCargar);
		
		JMenuItem mnMenuItemGuardar = new JMenuItem("Guardar");
		mnMenuItemGuardar.setSelected(true);
		mnMenu.add(mnMenuItemGuardar);
		
		mnMenuItemCargar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		cargarDatos();
        	}
        });

		mnMenuItemGuardar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		guardarDatos();
        	}
        });
		
		JLabel lablCoef = new JLabel("");
		editarLabelConImagen(lablCoef, getFramePrincipal(), "/imagenes/grey_bg.png", 622, 11, 155, 96);
		
		JLabel lblPelota = new JLabel("");
		editarLabelConImagen(lblPelota, getFramePrincipal(), "/imagenes/brazuca.png", 719, 379, 65, 72);
		
		JLabel lblCancha = new JLabel("");
		editarLabelConImagen(lblCancha, getFramePrincipal(), "/imagenes/footballpitch.png", 21, 41, 381, 399);
		
		JLabel lblFondo = new JLabel("");
		editarLabelConImagen(lblFondo, getFramePrincipal(), "/imagenes/fondo_circulo_central800x480.jpg", 0, 0, 800, 480);
	}

	public void configurarTabla(JTable tabla) {
		tabla.setEnabled(false);
 		tabla.setForeground(new Color(255, 255, 255));
 		tabla.setBackground(new Color(0, 128, 0));
 		tabla.setFont(new Font("Trebuchet MS", Font.PLAIN, 11));
 		dtcr = new DefaultTableCellRenderer();
		dtcr.setHorizontalAlignment(JLabel.CENTER);
		
		int[] anchos = {160,36,39,50};
		
		for (int columna = 0; columna < 4; columna++) {
			tabla.getColumnModel().getColumn(columna).setResizable(false);
			tabla.getColumnModel().getColumn(columna).setPreferredWidth(anchos[columna]);
			if (columna != 0){
				tabla.getColumnModel().getColumn(columna).setMaxWidth(anchos[columna]);
				tabla.getColumnModel().getColumn(columna).setCellRenderer(dtcr);
			}
		}		
	}
	
	public void editarLabelConImagen(JLabel label, JFrame f, String ruta, int arg1, int arg2, int arg3, int arg4) {
		label.setIcon(new ImageIcon(InterfazGrafica.class.getResource(ruta)));
		label.setBounds(arg1, arg2, arg3, arg4);
		f.getContentPane().add(label);
	}
	
	public void editarLabel(JLabel label,JFrame f, int arg1, int arg2, int arg3, int arg4, String texto, int color1, int color2, int color3) {
		if(f.getTitle().equals("Equipo Ideal del Mundial")){
			label.setVisible(false);
			label.setHorizontalTextPosition(SwingConstants.RIGHT);
			label.setHorizontalAlignment(SwingConstants.CENTER);
			label.setFont(new Font("Tahoma", Font.BOLD, 8));
		}
		else {
			label.setText(texto);
		}
		label.setForeground(new Color(color1, color2, color3));
		label.setBounds(arg1, arg2, arg3, arg4);
		f.getContentPane().add(label);
	}
	
	private void editarBoton(JButton boton, JFrame f, int arg1, int arg2, int arg3, int arg4) {
		boton.setBounds(arg1, arg2, arg3, arg4);
		if (f.getTitle().equals("Equipo Ideal del Mundial"))
			boton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		else
			boton.setFont(new Font("Tahoma", Font.BOLD, 11));
		f.getContentPane().add(boton);
	}
	
	public void editarCheckBox(JCheckBox check, JFrame f, String texto, int arg1, int arg2, int arg3, int arg4) {
		check.setBounds(arg1, arg2, arg3, arg4);
		check.setText(texto);
		f.getContentPane().add(check);
	}
	
	public void editarTextField(JTextField campo, JFrame f, int arg1, int arg2, int arg3, int arg4) {
		campo.setBounds(arg1, arg2, arg3, arg4);
		f.getContentPane().add(campo);
	}
	
	public void crearCamiseta(JLabel label, int arg1, int arg2, int arg3, int arg4) {
		label.setVisible(false);
		label.setBounds(arg1, arg2, arg3, arg4);
		contentPane.add(label);
	}
	
	private void dibujarJugador(String posicion, ArrayList<Jugador> mejorOnce, JLabel camiseta, JLabel nombre, JLabel coeficiente) {
		camiseta.setVisible(true);
		camiseta.setIcon(new ImageIcon(InterfazGrafica.class.getResource(rutaDeCamisetaSegunPosicion(posicion,mejorOnce))));
		nombre.setVisible(true);
		nombre.setText(nombreSegunPosicion(posicion,mejorOnce));
		coeficiente.setVisible(true);
		coeficiente.setText(coeficienteEnString(posicion,mejorOnce));
	} 
	
	private void agregarJugador(){
		desabilitarVentanaPrincipal();
		frameAgregarJugador = new JFrame();
		getFrameAgregarJugador().setSize(800, 480);
		getFrameAgregarJugador().setResizable(false);
		getFrameAgregarJugador().setTitle("Agregar Jugador");
		getFrameAgregarJugador().setIconImage(new ImageIcon(getClass().getResource("/imagenes/futbolista_icon.png")).getImage());
		getFrameAgregarJugador().getContentPane().setLayout(null);
		getFrameAgregarJugador().setLocationRelativeTo(null);
		
		JLabel lblNombre = new JLabel();
		editarLabel(lblNombre, getFrameAgregarJugador(), 20, 30, 70, 20, "Nombre:", 255, 69, 0);
		
		JTextField txtNomb = new JTextField();
		editarTextField(txtNomb, getFrameAgregarJugador(), 90, 30, 150, 20);
		
		JLabel lblApellido = new JLabel();
		editarLabel(lblApellido, getFrameAgregarJugador(), 20, 80, 70, 20, "Apellido:", 255, 69, 0);
		
		JTextField txtApellido = new JTextField();
		editarTextField(txtApellido, getFrameAgregarJugador(), 90, 80, 150, 20);
		
		JLabel lblNacionalidad = new JLabel();
		editarLabel(lblNacionalidad, getFrameAgregarJugador(), 20, 130, 80, 20, "Nacionalidad:", 255, 69, 0);
		
		JComboBox<String> comboNacionalidad = new JComboBox<String>();
		comboNacionalidad.setBounds(120, 130, 170, 20);
		cargarComboBoxNacionalidad(comboNacionalidad);
		getFrameAgregarJugador().getContentPane().add(comboNacionalidad);
		
		JLabel lblPosicion = new JLabel();
		editarLabel(lblPosicion, getFrameAgregarJugador(), 330, 30, 130, 20, "Posiciones:", 255, 69, 0);
		
		JCheckBox checkArquero = new JCheckBox();
		editarCheckBox(checkArquero, getFrameAgregarJugador(), "Arquero", 550, 30, 80, 20);
		
		JCheckBox checkCentralIzq = new JCheckBox();
		editarCheckBox(checkCentralIzq, getFrameAgregarJugador(), "Central Izquierdo", 590, 70, 130, 20);
		
		JCheckBox checkCentralDer = new JCheckBox();
		editarCheckBox(checkCentralDer, getFrameAgregarJugador(), "Central Derecho", 470, 70, 120, 20);
		
		JCheckBox checkLateralIzq = new JCheckBox();
		editarCheckBox(checkLateralIzq, getFrameAgregarJugador(), "Lateral Izquierdo", 650, 110, 120, 20);
		
		JCheckBox checkLateralDer = new JCheckBox();
		editarCheckBox(checkLateralDer, getFrameAgregarJugador(), "Lateral Derecho", 410, 110, 120, 20);
		
		JCheckBox checkVolanteCentr = new JCheckBox();
		editarCheckBox(checkVolanteCentr, getFrameAgregarJugador(), "Volante Central", 530, 160, 120, 20);

		JCheckBox checkVolanteIzq = new JCheckBox();
		editarCheckBox(checkVolanteIzq, getFrameAgregarJugador(), "Volante Izquierdo", 650, 200, 130, 20);
		
		JCheckBox checkVolanteDer = new JCheckBox();
		editarCheckBox(checkVolanteDer, getFrameAgregarJugador(), "Volante Derecho", 410, 200, 120, 20);
		
		JCheckBox checkCentrDel = new JCheckBox();
		editarCheckBox(checkCentrDel, getFrameAgregarJugador(), "Centro Delantero", 530, 310, 130, 20);
		
		JCheckBox checkPunteroIzq = new JCheckBox();
		editarCheckBox(checkPunteroIzq, getFrameAgregarJugador(), "Puntero Izquierdo", 650, 280, 130, 20);

		JCheckBox checkPunteroDer = new JCheckBox();
		editarCheckBox(checkPunteroDer, getFrameAgregarJugador(), "Puntero Derecho", 410, 280, 130, 20);

		JLabel cantidadGoles = new JLabel();
		editarLabel(cantidadGoles, getFrameAgregarJugador(), 20, 180, 110, 20, "Cantidad de goles:", 255, 69, 0);
		
		JTextField txtCantGoles = new JTextField();
		editarTextField(txtCantGoles, getFrameAgregarJugador(), 130, 180, 150, 20);
		
		JLabel cantidadFaltas = new JLabel();
		editarLabel(cantidadFaltas, getFrameAgregarJugador(), 20, 230, 110, 20, "Cantidad de faltas:", 255, 69, 0);
		
		JTextField txtCantFaltas = new JTextField();
		editarTextField(txtCantFaltas, getFrameAgregarJugador(), 130, 230, 150, 20);
		
		JLabel puntajePromedio = new JLabel();
		editarLabel(puntajePromedio, getFrameAgregarJugador(), 20, 280, 110, 20, "Puntaje Promedio:", 255, 69, 0);
		
		JTextField txtPuntajePromedio = new JTextField();
		editarTextField(txtPuntajePromedio, getFrameAgregarJugador(), 130, 280, 150, 20);
		
		JLabel cantidadTarjetas = new JLabel();
		editarLabel(cantidadTarjetas, getFrameAgregarJugador(), 20, 330, 180, 30, "Cantidad de tarjetas recibidas:", 255, 255, 255);
		
		JTextField txtCantTarjetas = new JTextField();
		editarTextField(txtCantTarjetas, getFrameAgregarJugador(), 200, 335, 150, 20);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		ArrayList<String> pos = new ArrayList<String>();
        		if(checkArquero.isSelected())pos.add("Arquero");
        		if(checkCentralIzq.isSelected())pos.add("Central izquierdo");
        		if(checkCentralDer.isSelected())pos.add("Central derecho");
        		if(checkLateralIzq.isSelected())pos.add("Lateral izquierdo");
        		if(checkLateralDer.isSelected())pos.add("Lateral derecho");
        		if(checkVolanteCentr.isSelected())pos.add("Volante central");
        		if(checkVolanteIzq.isSelected())pos.add("Volante izquierdo");
        		if(checkVolanteDer.isSelected())pos.add("Volante derecho");
        		if(checkCentrDel.isSelected())pos.add("Centro delantero");
        		if(checkPunteroIzq.isSelected())pos.add("Puntero izquierdo");
        		if(checkPunteroDer.isSelected())pos.add("Puntero derecho");
        		Jugador j = new Jugador(txtNomb.getText()+" "+txtApellido.getText(),comboNacionalidad.getSelectedItem().toString(),pos,
        				Integer.parseInt(txtCantGoles.getText()),Integer.parseInt(txtCantFaltas.getText()),Integer.parseInt(txtCantTarjetas.getText()),
        				Double.parseDouble(txtPuntajePromedio.getText()));
        		try{
        			seleccionador.agregarJugador(j);
        			tabbedPane.removeAll();
		        	JScrollPane scrollPaneMXI = new JScrollPane();
		     		tabbedPane.addTab("MejorXI", null, scrollPaneMXI, null);
		        	scrollPaneMXI.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		     		scrollPaneMXI.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		     		
		     		cabeceraTabla = new String[] {"Nombre", "Goles", "Faltas", "Tarjetas",};
		    		
		    		datosTablaMejorOnce = new String[][] {{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},{"", "", "", ""}, {"", "", "", ""}, 
		    		{"", "", "", ""},{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},};
		    		
		    		datosTablaSeleccion = new String[][] {{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, 
		    		{"", "", "", ""},{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},
		    		{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},{"", "", "", ""}, 
		    		{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},};			
		    								
		    		DefaultTableModel modelo = new DefaultTableModel();												
		    		tablaMejorXI = new JTable(modelo);
		    		tablaMejorXI.setModel(new DefaultTableModel(datosTablaMejorOnce, cabeceraTabla));
		    		configurarTabla(tablaMejorXI);
		     		
		    		scrollPaneMXI.setViewportView(tablaMejorXI);
	        	
		     		for(String seleccion : seleccionador.getSelecciones()){

	        			JScrollPane scrollPane = new JScrollPane();
	            		tabbedPane.addTab(seleccion, null, scrollPane, null);
	            		
	            		scrollPane.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
	            		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	            		
	            		DefaultTableModel model = new DefaultTableModel();	
	            		tablaSeleccion = new JTable(model);
	            		tablaSeleccion.setModel(new DefaultTableModel(datosTablaSeleccion, cabeceraTabla));
	            		configurarTabla(tablaSeleccion);
	            		
	            		scrollPane.setViewportView(tablaSeleccion);
          		
	            		ArrayList<Jugador> conjuntoSelec = seleccionador.dameTodosJugadoresDeSeleccion(seleccion);
	            		int Fjug = 0;
	            		for(Jugador jug : conjuntoSelec){
	            			tablaSeleccion.getModel().setValueAt(jug.getNombre(), Fjug, 0);
	            			tablaSeleccion.getModel().setValueAt(jug.getCantidadDeGoles(), Fjug, 1);
	            			tablaSeleccion.getModel().setValueAt(jug.getCantidadDeFaltas(), Fjug, 2);
	            			tablaSeleccion.getModel().setValueAt(jug.getCantidadDeTarjetasRecibidas(), Fjug, 3);
	            			Fjug++;
	            		}
	        		}        			
        			habilitarVentanaPrincipal();
        			getFrameAgregarJugador().dispose();
        		}catch(RuntimeException ex){
        		JOptionPane.showMessageDialog(null, "El jugador ya existe o supera el maximo de jugadores por seleccion", "Error al agregar jugador",2);
        		}catch(Exception ex){
        			JOptionPane.showMessageDialog(null, ex);
        		}
        	}
        });
		btnAceptar.setEnabled(false);
		editarBoton(btnAceptar, getFrameAgregarJugador(), 500, 420, 130, 20);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		habilitarVentanaPrincipal();
        		getFrameAgregarJugador().dispose();
        	}
        });
		editarBoton(btnCancelar, getFrameAgregarJugador(), 650, 420, 130, 20);

		txtNomb.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}		
		});
		
		txtApellido.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}		
		});
		
		comboNacionalidad.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
	
		checkArquero.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
		
		checkCentralIzq.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
		
		checkCentralDer.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
		
		checkLateralIzq.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
		
		checkLateralDer.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
		
		checkVolanteCentr.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
		
		checkVolanteIzq.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
		
		checkVolanteDer.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
		
		checkCentrDel.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
		
		checkPunteroIzq.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
		
		checkPunteroDer.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged( ItemEvent evento ){
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
		});
		
		txtCantGoles.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
			
			@Override
			public void keyTyped(KeyEvent e) {
				char caracter = e.getKeyChar();
				if(((caracter < '0') ||	(caracter > '9')) && (caracter != '\b')){
					JOptionPane.showMessageDialog(null, "solo se pueden ingresar valores numericos enteros", "Caracter invalido", 1);
					e.consume();
				}
			}
		});
		
		txtCantFaltas.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
			
			@Override
			public void keyTyped(KeyEvent e) {
				char caracter = e.getKeyChar();
				if(((caracter < '0') ||
						(caracter > '9')) &&
						(caracter != '\b')){
					JOptionPane.showMessageDialog(null, "solo se pueden ingresar valores numericos enteros", "Caracter invalido", 1);
					e.consume();
				}
			}
		});
		
		txtPuntajePromedio.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
			
			@Override
			public void keyTyped(KeyEvent e) {
				char caracter = e.getKeyChar();
				if(((caracter < '0') ||(caracter > '9')) && (caracter != '.') && (caracter != '\b')){
					JOptionPane.showMessageDialog(null, "solo valores numericos", "Caracter invalido", 1);
					e.consume();
				}else if(caracter=='.'){
					int cantidadDePuntos = 0;
					for(int i=0;i<txtPuntajePromedio.getText().length();i++){
						if(txtPuntajePromedio.getText().charAt(i)=='.'){
							cantidadDePuntos++;
						}
					}
					if(cantidadDePuntos>0){
						JOptionPane.showMessageDialog(null, "solo se puede colocar un punto para separar los decimales", "Caracter invalido", 1);
						e.consume();
					}
					if(cantidadDePuntos==0&&txtPuntajePromedio.getText().length()==0){
						JOptionPane.showMessageDialog(null, "no se puede colocar un punto si no es para separar decimales", "Caracter invalido", 1);
						e.consume();
					}
				}
			}
		});
		
		txtCantTarjetas.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(completoCamposDeJugador(txtNomb,txtApellido, comboNacionalidad,checkArquero,checkCentralIzq,checkCentralDer,checkLateralIzq,
				checkLateralDer, checkVolanteCentr,checkVolanteIzq,checkVolanteDer,checkCentrDel,checkPunteroIzq,checkPunteroDer, txtCantGoles,
				txtCantFaltas,txtPuntajePromedio,txtCantTarjetas)){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
			
			@Override
			public void keyTyped(KeyEvent e) {
				char caracter = e.getKeyChar();
				if(((caracter < '0') || (caracter > '9')) && (caracter != '\b')){
					JOptionPane.showMessageDialog(null, "solo se pueden ingresar valores numericos enteros", "Caracter invalido", 1);
					e.consume();
				}
			}
		});
		
		getFrameAgregarJugador().addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				habilitarVentanaPrincipal();
			}
		});
				
		JLabel fCancha = new JLabel();	
		editarLabelConImagen(fCancha, getFrameAgregarJugador(), "/imagenes/footballpitch.png", 400, 20, 400, 400);;
		
		JLabel fFondo = new JLabel();
		editarLabelConImagen(fFondo, getFrameAgregarJugador(), "/imagenes/fondo800x480.jpg", 0, 0, 800, 480);
		
		getFrameAgregarJugador().setVisible(true);
	}
	
	private boolean completoCamposDeJugador(JTextField txtNomb,JTextField txtApellido,JComboBox<String> comboNacionalidad,JCheckBox checkArquero,
		JCheckBox checkCentralIzq,JCheckBox checkCentralDer,JCheckBox checkLateralIzq,JCheckBox checkLateralDer,JCheckBox checkVolanteCentr,
		JCheckBox checkVolanteIzq,JCheckBox checkVolanteDer,JCheckBox checkCentrDel,JCheckBox checkPunteroIzq,JCheckBox checkPunteroDer,
		JTextField txtCantGoles,JTextField txtCantFaltas,JTextField txtPuntajePromedio,JTextField txtCantTarjetas){
		
			return txtNomb.getText().length()>0 && txtApellido.getText().length()>0 && comboNacionalidad.getSelectedIndex()>0
		    && txtCantGoles.getText().length()>0 && txtCantFaltas.getText().length()>0&&txtPuntajePromedio.getText().length()>0
			&& txtCantTarjetas.getText().length()>0 && (checkArquero.isSelected() || checkCentralIzq.isSelected()||checkCentralDer.isSelected()
			||checkLateralIzq.isSelected() || checkLateralDer.isSelected() || checkVolanteCentr.isSelected() || checkVolanteIzq.isSelected()
			||checkVolanteDer.isSelected() || checkCentrDel.isSelected()|| checkPunteroIzq.isSelected() || checkPunteroDer.isSelected());
	}
	
	private void cargarComboBoxNacionalidad(JComboBox<String> n){
		n.addItem("Seleccione la nacionalidad");
		for(int i=0;i<this.seleccionador.getSelecciones().size();i++)
			n.addItem(this.seleccionador.getSelecciones().get(i));
	}
	
	private void habilitarVentanaPrincipal(){
		getFramePrincipal().setEnabled(true);
	}
	
	private void desabilitarVentanaPrincipal(){
		getFramePrincipal().setEnabled(false);
	}
	
	private void agregarSeleccion(){
		desabilitarVentanaPrincipal();
		JFrame frameAgregarSelec = new JFrame();
		frameAgregarSelec.setSize(400, 160);
		frameAgregarSelec.setResizable(false);
		frameAgregarSelec.setTitle("Agregar Seleccion");
		frameAgregarSelec.setIconImage(new ImageIcon(getClass().getResource("/imagenes/iconoAgregarSeleccion.png")).getImage());
		frameAgregarSelec.getContentPane().setLayout(null);
		frameAgregarSelec.setLocationRelativeTo(null);
		
		JLabel lblNombreSelec = new JLabel();	
		editarLabel(lblNombreSelec, frameAgregarSelec, 45, 30, 70, 20, "Pais:", 0, 0, 0);
				
		JTextField txtNombS = new JTextField();
		editarTextField(txtNombS, frameAgregarSelec, 90, 30, 150, 20);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(100, 90, 130, 20);
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {  
		        try{
		        	seleccionador.agregarSeleccion(txtNombS.getText());
		        	 tabbedPane.removeAll();
		        	 
		        	JScrollPane scrollPaneMXI = new JScrollPane();
		     		tabbedPane.addTab("MejorXI", null, scrollPaneMXI, null);
		        	scrollPaneMXI.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		     		scrollPaneMXI.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		     		
		     		cabeceraTabla = new String[] {"Nombre", "Goles", "Faltas", "Tarjetas",};
		    		
		     		datosTablaMejorOnce = new String[][] {{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},{"", "", "", ""}, {"", "", "", ""}, 
		     		{"", "", "", ""},  {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},};

		     		datosTablaSeleccion = new String[][] {{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},
		     		{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},
				    {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, 
				    {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},};						
		    		
					DefaultTableModel modelo = new DefaultTableModel();											
		    		tablaMejorXI = new JTable(modelo);		    		
		    		tablaMejorXI.setModel(new DefaultTableModel(datosTablaMejorOnce, cabeceraTabla));
		    		configurarTabla(tablaMejorXI);
		     		
		    		scrollPaneMXI.setViewportView(tablaMejorXI);
   	         	 
		        	for(String seleccion : seleccionador.getSelecciones()){

	        			JScrollPane scrollPane = new JScrollPane();
	            		tabbedPane.addTab(seleccion, null, scrollPane, null);
	            		
	            		scrollPane.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
	            		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	            		
	            		DefaultTableModel model = new DefaultTableModel();	
	            		tablaSeleccion = new JTable(model);
	            		tablaSeleccion.setModel(new DefaultTableModel(datosTablaSeleccion, cabeceraTabla));
	            		configurarTabla(tablaSeleccion);     
	            		
	            		scrollPane.setViewportView(tablaSeleccion);
         		
	            		ArrayList<Jugador> conjuntoSelec = seleccionador.dameTodosJugadoresDeSeleccion(seleccion);
	            		int Fjug = 0;
	            		for(Jugador jug : conjuntoSelec){
	            			tablaSeleccion.getModel().setValueAt(jug.getNombre(), Fjug, 0);
	            			tablaSeleccion.getModel().setValueAt(jug.getCantidadDeGoles(), Fjug, 1);
	            			tablaSeleccion.getModel().setValueAt(jug.getCantidadDeFaltas(), Fjug, 2);
	            			tablaSeleccion.getModel().setValueAt(jug.getCantidadDeTarjetasRecibidas(), Fjug, 3);
	            			Fjug++;
	            		}	            		
	        		}
		        	habilitarVentanaPrincipal();
		        	frameAgregarSelec.dispose();
        		}catch(RuntimeException ex){
        	JOptionPane.showMessageDialog(null, "La seleccion ya existe o supera el maximo de selecciones por mundial", "Error al agregar seleccion",2);
        		}catch(Exception ex){
        			JOptionPane.showMessageDialog(null, ex);
        		}
		    }
		});
		btnAceptar.setEnabled(false);
		frameAgregarSelec.getContentPane().add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(250, 90, 130, 20);
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		      	habilitarVentanaPrincipal();
		      	frameAgregarSelec.dispose();
		    }
		});
		frameAgregarSelec.getContentPane().add(btnCancelar);		
		
		JLabel lblFondoSelec = new JLabel();
		editarLabelConImagen(lblFondoSelec, frameAgregarSelec, "/imagenes/banderas400x160.jpg", 0, 0, 400, 160);	
			
		txtNombS.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(txtNombS.getText().length()>0){
					btnAceptar.setEnabled(true);
				}
				else btnAceptar.setEnabled(false);
			}
			
			@Override
			public void keyTyped(KeyEvent e) {
				char caracter = e.getKeyChar();
				if((caracter != '\b')&&(caracter==' ')){
					JOptionPane.showMessageDialog(null, "no se pueden ingresar espacios");
					e.consume();
				}
			}		
		});
		
		frameAgregarSelec.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				habilitarVentanaPrincipal();
			}
		});
		frameAgregarSelec.setVisible(true);
	}
		
	private void cargarDatos(){
		JFileChooser dig = new JFileChooser(new File("").getAbsolutePath()+"\\archivosGuardados");
		FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos JSON", "JSon");
		dig.setFileFilter(filtro);
		
		int option = dig.showOpenDialog(getFramePrincipal());
        if(option == JFileChooser.APPROVE_OPTION){
        	this.seleccionador = Seleccionador.cargarArchivoGuardado(dig.getSelectedFile().getAbsolutePath());
        	
        	tabbedPane.removeAll();
   	 
        	JScrollPane scrollPaneMXI = new JScrollPane();
     		tabbedPane.addTab("MejorXI", null, scrollPaneMXI, null);
        	scrollPaneMXI.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
     		scrollPaneMXI.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
     		
     		cabeceraTabla = new String[] {"Nombre", "Goles", "Faltas", "Tarjetas",};
    		
     		datosTablaMejorOnce = new String[][] {{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},{"", "", "", ""}, {"", "", "", ""}, 
     		{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},};

            datosTablaSeleccion = new String[][] {{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},
            {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},{"", "", "", ""},
            {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},{"", "", "", ""}, {"", "", "", ""}, {"", "", "", ""},
            {"", "", "", ""}, {"", "", "", ""},};			
    								
			DefaultTableModel modelo = new DefaultTableModel();						
			tablaMejorXI = new JTable(modelo);
			tablaMejorXI.setModel(new DefaultTableModel(datosTablaMejorOnce, cabeceraTabla));						
			configurarTabla(tablaMejorXI);
     		
			scrollPaneMXI.setViewportView(tablaMejorXI);
    	       	 
        	for(String seleccion : seleccionador.getSelecciones()){

    			JScrollPane scrollPane = new JScrollPane();
        		tabbedPane.addTab(seleccion, null, scrollPane, null);
        		
        		scrollPane.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
        		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        		
        		DefaultTableModel model = new DefaultTableModel();	
        		tablaSeleccion = new JTable(model);
        		tablaSeleccion.setModel(new DefaultTableModel(datosTablaSeleccion, cabeceraTabla));
        		configurarTabla(tablaSeleccion);
        		
        		scrollPane.setViewportView(tablaSeleccion);

        		ArrayList<Jugador> conjuntoSelec = seleccionador.dameTodosJugadoresDeSeleccion(seleccion);
        		int Fjug = 0;
        		for(Jugador jug : conjuntoSelec){
        			tablaSeleccion.getModel().setValueAt(jug.getNombre(), Fjug, 0);
        			tablaSeleccion.getModel().setValueAt(jug.getCantidadDeGoles(), Fjug, 1);
        			tablaSeleccion.getModel().setValueAt(jug.getCantidadDeFaltas(), Fjug, 2);
        			tablaSeleccion.getModel().setValueAt(jug.getCantidadDeTarjetasRecibidas(), Fjug, 3);
        			Fjug++;
        		}    		
    		}     	       	
        }
	}

	private void guardarDatos(){
		String valor = JOptionPane.showInputDialog("ingrese un nombre para el archivo");
		if (valor != null) {
			if(valor.length() > 0) 
				this.seleccionador.guardar("archivosGuardados\\"+valor+".JSon");
			
			else JOptionPane.showMessageDialog(null, "No se puede guardar un archivo sin nombre", "Error al guardar", 2);
		}
	}
	
	private String rutaDeCamisetaSegunPosicion(String posicion, ArrayList<Jugador> MXI){
		String camiseta = "/imagenes/camiseta_neutral.png";
		int pos = damePosicion(posicion);
		
		if (MXI.get(pos).getNacionalidad().equals("Argentina")){
			camiseta = "/imagenes/camiseta_argentina.png";
		}
		else if (MXI.get(pos).getNacionalidad().equals("Alemania")){
			camiseta = "/imagenes/camiseta_alemania.png";
		}
		else if (MXI.get(pos).getNacionalidad().equals("Brasil")){
			camiseta = "/imagenes/camiseta_brasil.png";
		}
		else if (MXI.get(pos).getNacionalidad().equals("Holanda")){
			camiseta = "/imagenes/camiseta_holanda.png";
		}
		else if (MXI.get(pos).getNacionalidad().equals("Colombia")){
			camiseta = "/imagenes/camiseta_colombia.png";
		}
		else if (MXI.get(pos).getNacionalidad().equals("Belgica")){
			camiseta = "/imagenes/camiseta_belgica.png";
		}
		else if (MXI.get(pos).getNacionalidad().equals("Espana")){
			camiseta = "/imagenes/camiseta_espana.png";
		}
		else if (MXI.get(pos).getNacionalidad().equals("Francia")){
			camiseta = "/imagenes/camiseta_francia.png";
		}
		else if (MXI.get(pos).getNacionalidad().equals("Portugal")){
			camiseta ="/imagenes/camiseta_portugal.png";
		}
		else if (MXI.get(pos).getNacionalidad().equals("Inglaterra")){
			camiseta ="/imagenes/camiseta_inglaterra.png";
		}	
		return camiseta;
	}
	
	private String nombreSegunPosicion(String posicion,ArrayList<Jugador>MXI) {
		int pos = damePosicion(posicion);
		return MXI.get(pos).getNombre();
	}
	
	public String coeficienteEnString(String posicion,ArrayList<Jugador>MXI){
		double coef;
		int pos = damePosicion(posicion);
		coef = MXI.get(pos).getCoeficiente();
		return String.valueOf(coef);	
	}
	
public int damePosicion(String posicion){
		
		String[] posiciones = new String[] {"lateralIzquierdo","centralIzquierdo","centralDerecho","lateralDerecho","volanteIzquierdo","volanteCentral","volanteDerecho",
		"punteroIzquierdo","centroDelantero","punteroDerecho"};
		int pos = 0;
		
		for (int i = 0; i < posiciones.length; i++) { 
			if (posicion.equals(posiciones[i])) {
				pos = i + 1 ; 
			}
		}
		return pos;
	}
	
	public JFrame getFramePrincipal() {
		return framePrincipal;
	}
	
	public JFrame getFrameAgregarJugador() {
		return frameAgregarJugador;
	}
}