package mundialMejorOnce;


import java.util.Comparator;
/* Clase comparador
 * Compara dos objetos, (en nuestro caso jugadores) para hacer el collection.sort en el seleccionador. Es decir para ordenar de mejor a peor jugador.
 */
public class Comparador implements Comparator<Object> {

	@Override
	public int compare(Object arg0, Object arg1) {
		
		if(((Jugador)arg0).getCoeficiente() > ((Jugador)arg1).getCoeficiente() ) {
			return -1;
		}else if ( ((Jugador)arg0).getCoeficiente() == ((Jugador)arg1).getCoeficiente()) {
			return 0;
		}else {
			return 1;
		}
	}

}
