package test;
import static org.junit.Assert.*;
import java.util.ArrayList;
import mundialMejorOnce.Jugador;
import mundialMejorOnce.LectorDeDatos;
import mundialMejorOnce.Seleccionador;

import org.junit.Before;
import org.junit.Test;


public class testMejorOnce {
	ArrayList<Jugador> todosLosJugadores;
	Seleccionador seleccionador;
	LectorDeDatos lector;
	Jugador messi;
	Jugador yo;
	ArrayList<String> posicionesQueJuega;
	String [] posiciones;
	
	@Before
	public void setUp() throws Exception {
		todosLosJugadores = new ArrayList<>();
		seleccionador = new Seleccionador (todosLosJugadores);
		lector = new LectorDeDatos(seleccionador);
		String [] selecciones = {"Alemania","Argentina","Belgica","Brasil","Colombia","Espa�a","Francia","Holanda","Inglaterra","Portugal"};
		posiciones = new String[] {"Arquero","Lateral izquierdo","Central izquierdo","Central derecho","Lateral derecho","Volante izquierdo",
				"Volante central","Volante derecho","Puntero izquierdo","Centrodelantero","Puntero derecho"};
		for(String nacionalidad : selecciones) {
			lector.setDatos(nacionalidad);
			lector.getSeleccionador().agregarSeleccion(nacionalidad);
		}
		String[] selecciones2 = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v"};
		for(String seleccion : selecciones2) {
			seleccionador.agregarSeleccion(seleccion);
		}
		seleccionador = lector.getSeleccionador();
		posicionesQueJuega = new ArrayList<>();
		messi = new Jugador("Lionel Messi", "Argentina", posicionesQueJuega, 1, 1, 1, 1);
		yo = new Jugador("Juan Manuel Losada","Argentina", posicionesQueJuega, 10, 0, 0, 10);
	}

	@Test (expected = RuntimeException.class)
	public void testAgregarMessi() {
		ArrayList<String> posicionesQueJuega = new ArrayList<>();
		posicionesQueJuega.add("Puntero derecho");
		seleccionador.agregarJugador(messi);
	}
	
	@Test(expected = RuntimeException.class)
	public void testAgregarArgentino() {
		ArrayList<String> posicionesQueJuega = new ArrayList<>();
		posicionesQueJuega.add("Arquero");
		seleccionador.agregarJugador(yo);
	}
	
	@Test (expected = RuntimeException.class)
	public void testSeleccionExistente() {
		seleccionador.agregarSeleccion("Argentina");
	}
	
	@Test (expected = RuntimeException.class)
	public void test33Selecciones() {
		seleccionador.agregarSeleccion("aaaa");
	}
	
	@Test
	public void testExisteJugador() {
		assertTrue(seleccionador.existeJugador(messi));
	}
	
	@Test
	public void testNoExisteJugador() {
		assertFalse(seleccionador.existeJugador(yo));
	}
	
	@Test
	public void cantJugadoresSeleccion() {
		assertEquals(23,seleccionador.cantJugadoresDeSeleccion("Argentina"));
	}
	
	@Test
	public void cantJugadoresSeleccionVacia() {
		assertEquals(0,seleccionador.cantJugadoresDeSeleccion("a"));
	}
	
	@Test
	public void testOrdenarCandidatos() {
		int cont = 0;
		for(ArrayList<Jugador> jugadoresOrdenados : seleccionador.getCandidatosOrdenados()) {
			for(int i = 0; i < jugadoresOrdenados.size()-1; i++) {
				if(jugadoresOrdenados.get(i).getCoeficiente() >= jugadoresOrdenados.get(i+1).getCoeficiente()) {
					cont++;
				}
			}
		}
		assertEquals(seleccionador.cantJugadoresOrdenados(),cont);
	}
}
